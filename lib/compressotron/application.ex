defmodule Compressotron.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    IO.puts "Starting service on port: #{Application.get_env(:compressotron, :http_port)}"
    IO.puts "ffmpeg bin: #{Application.get_env(:compressotron, :ffmpeg_bin)}"
    children = [
      # Starts a worker by calling: Compressotron.Worker.start_link(arg)
      {Compressotron, %{}},
      # num_acceptors: 3 for no reason other than to make the observer easier to read. It should just be left at default=100
      {Plug.Cowboy, scheme: :http, plug: CompServ, options: [port: Application.get_env(:compressotron, :http_port), transport_options: [num_acceptors: 3]]}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Compressotron.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
