defmodule Compressotron do
  use Supervisor
  
  def start_link(init_args) do
    Supervisor.start_link(__MODULE__, init_args, name: __MODULE__)
  end

  def init(_init_arg) do
    children = [
      {FFmpegWorker, %{}},
      {Task.Supervisor, name: FFmpegWorker.TaskSupervisor}
    ]
    Supervisor.init(children, strategy: :one_for_one)
  end
end
