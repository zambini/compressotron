defmodule CompServ do
  # import Plug.Conn

  # def init(opts) do
  #   opts
  # end

  # def call(conn, _opts) do
  #   conn |> put_resp_content_type("text/plain") |> send_resp(200, "Hi")
  # end
  use Plug.Router

  @one_gigabyte 1_073_741_824
  @content_type_json "application/json"

  plug Plug.Parsers, parsers: [:urlencoded, {:multipart, length: @one_gigabyte}]
  plug Plug.Static, at: "/public", from: FFmpegWorker.local_root_storage
  plug Plug.Static, at: "/", from: :compressotron, only: ["favicon.ico"], headers: %{"access-control-allow-origin"=> "*"}
  plug :match
  plug :dispatch


  get "/" do
    conn
      |> put_resp_header("content-type", @content_type_json)
      |> send_resp(200, Jason.encode!(%{:message => "hi, see README"}))
  end

  get "/videos" do
    conn
      |> put_resp_header("content-type", @content_type_json)
      |> send_resp(200, Jason.encode!(%{:videos => FFmpegWorker.list_videos()}))
  end

  @doc """
  In lieu of hooking up with cloud storage (AWS S3, GCP, etc), just accept videos here and dump into
  /tmp (or the OS equivalent).
  """
  post "/videos" do
    %{body_params: params} = conn
    IO.inspect params
    if file = params["file"] do
      result = FFmpegWorker.capture_upload(file.path)
      # just return string to keep this simple
      send_resp(conn, 201, "#{result}")
    else
      # just return string to keep this simple
      send_resp(conn, 400, "Missing file parameter")
    end
  end

  match _ do
    conn
      |> put_resp_header("content-type", @content_type_json)
      |> send_resp(404, Jason.encode!(%{:message => "it looks like you've taken a wrong turn"}))
  end
end
