defmodule FFmpegWorker do
  use GenServer

  # Settings would be better off in their own module that can be reloaded w/o taking down this server
  @encoder_settings [
    high: %{
      height: 1080,
      width: 1920
    },
    med: %{
      height: 720,
      width: 1280
    },
    low: %{
      height: 480,
      width: 640
    }
  ]

  # Same for this
  def ffmpeg_bin, do: "#{Application.get_env(:compressotron, :ffmpeg_bin)}"
  # and all of these
  @capture_dir_name "captures"
  def local_root_storage, do: Path.join([System.tmp_dir!(), "compressotron"])
  def captures_dir, do: Path.join([local_root_storage(), @capture_dir_name])
  def local_folder_by_id(id), do: Path.join([local_root_storage(), "#{id}"])

  @doc """
  Initial state should be an empty map.
  Items will be a key/value pair of "current jobs" and their timestamps

  In reality, this would best be persisted outside of this genserver's state.
  """
  def init(state \\ %{}), do: {:ok, state}

  def start_link(state \\ %{}) do
    GenServer.start_link(__MODULE__, state, name: __MODULE__)
  end

  @doc """
  Helper to create a new process to handle FFmpeg job
  """
  def create_task(id, file_path, quality) do
    out_name = "processed_#{quality}.mp4"
    out_dir = local_folder_by_id(id)
    # Ensure the output dir exists no matter the job
    File.mkdir_p!(out_dir)
    %{height: height, width: width} = @encoder_settings[quality]
    IO.puts "Writing [#{width}x#{height}]to '#{Path.join(out_dir, out_name)}'"
    # FFmpeg *should* terminate on closing the ports, so zombie procs should be avoidable
    Task.Supervisor.async(FFmpegWorker.TaskSupervisor,
      fn -> {output, exit_code} = System.cmd(
        ffmpeg_bin(),
        [
          "-i",
          file_path,
          "-s",
          "#{width}x#{height}",
          Path.join(out_dir, out_name)
        ],
        parallelism: true, stderr_to_stdout: true
      )
      case exit_code do
        0 ->
          on_job_completed(id, quality)
        _ ->
          # Usually the last line printed in ffmpeg is a good indication of what caused the failure
          reason = String.split(output, ~r/\R/)
            |> Enum.map(fn x -> String.trim(x) end)
            |> Enum.filter(fn x -> String.length(x) > 0 end)
            |> List.last
          raise "Job #{id} failed with exit code=#{exit_code} reason=[ffmpeg:#{reason}]"
      end
    end
    )
  end

  # this would really use something like uuid or shortid for the id, not rand
  defp make_id, do: "#{:rand.uniform(1000000)}"

  # Server API
  def start_job(src_file, id) when id === nil do
    start_job(src_file, make_id())
  end
  
  def start_job(src_file, id) when id !== nil do
    create_task(id, src_file, :low)
    create_task(id, src_file, :med)
    create_task(id, src_file, :high)
    # state adds all jobs now instead of any delayed invocations
    id
  end
  
  def handle_call(:jobs, _from, state), do: {:reply, state, state}
  
  def handle_call({:add_job, src_file, id}, _from, state) do
    id = start_job(src_file, id)
    # state adds all jobs now instead of any delayed invocations
    {:reply, id, Map.put(state, id, %{src_file: src_file, high: DateTime.utc_now(), med: DateTime.utc_now(), low: DateTime.utc_now()})}
  end
  
  def handle_call({:capture_upload, uploaded_file}, _from, state) do
    # this would really use something like uuid or shortid for the id, not rand
    new_id = make_id()
    capture_dir = captures_dir()
    captured_filename = "upload_#{new_id}.mp4"
    target_absolute_path = Path.join([capture_dir, captured_filename])
    IO.puts "Capturing uploaded file=#{uploaded_file} to local=#{target_absolute_path}"
    File.mkdir_p!(capture_dir)
    File.cp!(uploaded_file, target_absolute_path)
    start_job(target_absolute_path, new_id)
    {:reply, new_id, Map.put(state, new_id, %{src_file: target_absolute_path, high: DateTime.utc_now(), med: DateTime.utc_now(), low: DateTime.utc_now()})}
  end

  def handle_call({:list_videos}, _from, state) do
    files = File.ls!(local_root_storage())
      |> Enum.filter(fn x -> x !== @capture_dir_name end)
      |> Enum.map(fn x -> %{
        low: "//public/#{x}/processed_low.mp4",
        med: "//public/#{x}/processed_med.mp4",
        high: "//public/#{x}/processed_high.mp4",
      } end)
    {:reply, files, state}
  end

  def handle_cast({:job_completed, id, quality}, state) do
    {time_start, rest} = pop_in(state, [id, quality])
    # {remaining_jobs, _} = Map.pop(rest, id)
    IO.puts "id=#{id} completed quality=#{quality} in #{DateTime.diff(DateTime.utc_now(), time_start)}s"
    # IO.inspect remaining_jobs
    # IO.puts "quality=#{quality} and !Map.has_key?(remaining_jobs, :med)=#{!Map.has_key?(remaining_jobs, :med)} and !Map.has_key?(remaining_jobs, :low)=#{!Map.has_key?(remaining_jobs, :low)}"
    # if quality !== :high and !Map.has_key?(remaining_jobs, :med) and !Map.has_key?(remaining_jobs, :low) do
    #   create_task(id, remaining_jobs[:src_file], :high)
    # end
    {:noreply, rest}
  end
  
  # Ignore successful task completion :ok
  def handle_info({_, :ok}, state) do
    IO.puts "Discarding :ok message"
    {:noreply, state}
  end

  # Ignore :normal down message (why does the task send two messages for success?)
  def handle_info({:DOWN, _, _, _, :normal}, state) do
    IO.puts "Discarding :normal :down message"
    {:noreply, state}
  end

  # Client API
  @doc """
  Add a new job. Input is the absolute path to the video
  """
  def add_job(value, id \\ nil), do: GenServer.call(__MODULE__, {:add_job, value, id})
  def capture_upload(value), do: GenServer.call(__MODULE__, {:capture_upload, value})
  def jobs, do: GenServer.call(__MODULE__, :jobs)
  def list_videos, do: GenServer.call(__MODULE__, {:list_videos})

  # Private client API
  defp on_job_completed(id, quality), do: GenServer.cast(__MODULE__, {:job_completed, id, quality})
end
