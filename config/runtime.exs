import Config

IO.puts "Configuration executed"

config :compressotron,
  ffmpeg_bin: System.get_env("FFMPEG_BIN", "C:/cygwin64/usr/local/etc/ffmpeg-20170723-dd4b7ba-win64-static/bin/ffmpeg.exe"),
  http_port: String.to_integer(System.get_env("PORT", "4001"))
