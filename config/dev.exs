import Config

IO.puts "Configuration executed"

config :compressotron,
  ffmpeg_bin: nil,
  http_port: 4001

# for anything that belongs in dev env
import_config "#{config_env()}.exs"
