# Compressotron

A very basic Elixir app to compress video files using `ffmpeg`

## Development

### Setup

This project uses Plug and Jason dependencies, listed in the `mix.exs`. Install it with:

```shell
λ mix deps.get
```

### Local Server

Launch the server with the following command

```shell
λ mix run --no-halt
```
or
```shell
λ iex -S mix
```

## API
This service has a very simple API. The point of this application is to receive one video and transcode it into 3 simple resolutions: 480p, 720p, 1080p (note: no intelligent aspect ratio handling here or even ffmpeg-auto scaling. Modify `FFmpegWorker.@encoder_settings` if using videos not conforming to `16:9`).

### `POST /videos` - Upload Video
> Sends a video to the transcoder service

```shell
λ curl -v -F 'file=@input.mp4' localhost:4001/videos
```

### `GET /videos` - Get Videos list
> Fetches a list of existing videos, uses dumb file list to generate

```shell
λ curl localhost:4001/videos
```

## Environment Variables

| Variable   | Mandatory | Description | Example |
|------------|-----------|-------------|---------|
| HTTP_PORT  | No        | HTTP Port to serve application [default: 4001] | `4001` |
| FFMPEG_BIN | **Yes**   | Absolute path to ffmpeg binary to execute video transcoding jobs (see: `runtime.exs` to manually change) | |

## FFmpeg binaries

You can find your FFmpeg binaries either by installing [FFmpeg](https://ffmpeg.org/download.html)
and then running `$ which ffmpeg` in Linux/OSX systems or by using static binaries
([linux here](https://johnvansickle.com/ffmpeg/)) for dependency free/virtual execution.
